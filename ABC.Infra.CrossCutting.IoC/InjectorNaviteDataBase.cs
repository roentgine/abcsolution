﻿using ABC.Core.Interfaces;
using ABC.Infrastructure.Data;
using ABC.Infrastructure.Data.Repository;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ABC.Infra.CrossCutting.IoC
{
    public class InjectorNaviteDataBase
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<ABCContext>();
            services.AddTransient<IABCClientRepository, ClientRepository>();
            services.AddTransient<IABCUserRepository, UserRepository>();
            services.AddTransient<IABCCredentialRepository, CredentialRepository>();
            services.AddTransient<IABCProductRepository, ProductRepository>();
            services.AddTransient<IABCOrdersRepository, OrderRepository>();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());


        }
    }
}