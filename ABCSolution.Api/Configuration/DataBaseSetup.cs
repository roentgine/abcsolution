﻿using ABC.Infra.CrossCutting.IoC;
using ABC.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ABCSolution.Api.Configuration
{
    public static class DataBaseSetup
    {
        public static void StartDataBase(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddDbContext<ABCContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("ABCConnection")));
            InjectorNaviteDataBase.RegisterServices(services);

        }

    }
}
