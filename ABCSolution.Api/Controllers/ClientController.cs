﻿using ABC.Core.DTO;
using ABC.Core.Entities;
using ABC.Core.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ABCSolution.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : Controller
    {
        private readonly IABCClientRepository _clientRepository;
        private readonly IMapper _mapper;

        public ClientController(IABCClientRepository clientRepository, IMapper mapper)
        {
            _clientRepository = clientRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [ApiVersion("1.0")]
        public async Task<IActionResult> GetClients()
        {
            var clients = await _clientRepository.GetClients();
            var clientDto = _mapper.Map<IEnumerable<ClientDTO>>(clients);
            return Ok(clientDto);
        }

        [HttpGet("{id}")]
        [ApiVersion("1.0")]
        public async Task<IActionResult> GetClient(int id)
        {
            var client = await _clientRepository.GetClient(id);
            var clientDto = _mapper.Map<ClientDTO>(client);
            return Ok(clientDto);
        }

        [HttpPost]
        [ApiVersion("1.0")]
        public async Task<IActionResult> InsertClient(InsertClientDTO in_client) 
        {
            var map_client = _mapper.Map<Client>(in_client);
            var client = await _clientRepository.InsertClient(map_client);
            var clientDto = _mapper.Map<ClientDTO>(client);
            return Ok(clientDto);
        }
    }
}
