﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ABC.Core.DTO;
using ABC.Core.Entities;
using ABC.Core.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace ABCSolution.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {

        private readonly IABCProductRepository _productRepository;
        private readonly IMapper _mapper;

        public ProductController(IMapper mapper, IABCProductRepository productRepository)
        {
            _productRepository = productRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [ApiVersion("1.0")]
        public async Task<IActionResult> GetProducts()
        {
            var products = await _productRepository.GetProducts();
            return Ok(products);
        }
        
        [HttpGet("{id}")]
        [ApiVersion("1.0")]
        public async Task<IActionResult> GetProduct(int id)
        {
            var products = await _productRepository.GetProduct(id);
            var map_product = _mapper.Map<ProductDTO>(products);
            return Ok(map_product);
        }

        [HttpPost]
        [ApiVersion("1.0")]
        public async Task<IActionResult> InsertProduct(InsertProductDTO in_product)
        {
            var in_data = _mapper.Map<Product>(in_product);
            var product = await _productRepository.InsertProduct(in_data);
            var map_product = _mapper.Map<ProductDTO>(product);
            return Ok(map_product);
        }

        
    }
}
