﻿using ABC.Core.DTO;
using ABC.Core.Entities;
using ABC.Core.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ABCSolution.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : Controller
    {
        private readonly IABCOrdersRepository _orderRepository;
        private readonly IABCClientRepository _clientRepository;
        private readonly IABCProductRepository _productRepository;
        private readonly IABCUserRepository _userRepository;
        private readonly IMapper _mapper;

        public OrderController(IABCUserRepository userRepository, IMapper mapper,
            IABCOrdersRepository orderRepository, IABCProductRepository productRepository,
            IABCClientRepository clientRepository)
        {
            _userRepository = userRepository;
            _orderRepository = orderRepository;
            _productRepository = productRepository;
            _clientRepository = clientRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [ApiVersion("1.0")]
        public async Task<IActionResult> GetOrders()
        {
            var orders = await _orderRepository.GetOrders();
            return Ok(orders);
        }

        [HttpPost]
        [ApiVersion("1.0")]
        public async Task<IActionResult> InsertOrders(InsertOrderDTO in_order)
        {
            var map_order = _mapper.Map<Orders>(in_order);
            var order = await _orderRepository.InsertOrder(map_order);
            var user = await _userRepository.GetUser(order.AbcUserId);
            var client = await _clientRepository.GetClient(order.ClientId);
            var product = await _productRepository.GetProduct(order.ProductId);
            await _productRepository.SetProduct(product.ProductId);
            var OrderDto = await _orderRepository.CustomMap(order, product, client, user);
            return Ok(OrderDto);

        }

        [HttpPut]
        [ApiVersion("1.0")]
        public async Task<IActionResult> DispatchOrder(DispatchOrderDTO in_order)
        {
            var order = await _orderRepository.DispatchOrder(in_order);
            var user = await _userRepository.GetUser(order.AbcUserId);
            var client = await _clientRepository.GetClient(order.ClientId);
            var product = await _productRepository.GetProduct(order.ProductId);
            var OrderDto = await _orderRepository.CustomMap(order, product, client, user);

            return Ok(OrderDto);

        }
    }
}
