﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ABC.Core.DTO;
using ABC.Core.Entities;
using ABC.Core.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace ABCSolution.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IABCUserRepository _userRepository;
        private readonly IABCCredentialRepository _credentialRepository;
        private readonly IMapper _mapper;

        public UserController(IABCUserRepository userRepository, IMapper mapper, IABCCredentialRepository credentialRepository)
        {
            _userRepository = userRepository;
            _credentialRepository = credentialRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [ApiVersion("1.0")]
        public async Task<IActionResult> GetUsers()
        {
            var users = await _userRepository.GetUsers();
            return Ok(users);
        }
        
        [HttpGet("{id}")]
        [ApiVersion("1.0")]
        public async Task<IActionResult> GetUser(int id)
        {
            var users = await _userRepository.GetUser(id);
            var user = _mapper.Map<UserDTO>(users);
            return Ok(user  );
        }
        
        
        [HttpPost]
        [ApiVersion("1.0")]
        public async Task<IActionResult> InsertUsers(InsertUserDTO user)
        {
            var in_credential = _mapper.Map<Credential>(user);
            int credentialid = await _credentialRepository.InsertCredential(in_credential);
            var in_user = await _userRepository.CustomMap(user, credentialid);
            var users = await _userRepository.InsertUser(in_user);
            var userData = _mapper.Map<UserDTO>(users);
            return Ok(userData);
        }
    }
}
