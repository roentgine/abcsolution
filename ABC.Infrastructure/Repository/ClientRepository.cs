﻿using ABC.Core.Interfaces;
using ABC.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Infrastructure.Data.Repository
{
    public class ClientRepository : IABCClientRepository
    {

        private readonly ABCContext _context;
        public ClientRepository(ABCContext context)
        {

            _context = context;

        }

        public async Task<IEnumerable<Client>> GetClients()
        {
            var Clients = await _context.Client.ToListAsync();
            return Clients;
        }

        public async Task<Client> GetClient(int id)
        {
            var client = await _context.Client.FirstOrDefaultAsync(c => c.ClientId == id);
            return client;
        }

        public async Task<Client> InsertClient(Client in_client) 
        {
            var client = _context.Client.Add(in_client);
            await _context.SaveChangesAsync();
            return client.Entity;
        }

    }
}
