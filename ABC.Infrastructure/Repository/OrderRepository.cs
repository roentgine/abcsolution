﻿using ABC.Core.Interfaces;
using ABC.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABC.Core.DTO;

namespace ABC.Infrastructure.Data.Repository
{
    public class OrderRepository : IABCOrdersRepository
    {

        private readonly ABCContext _context;
        public OrderRepository(ABCContext context)
        {

            _context = context;

        }

        public async Task<IEnumerable<Orders>> GetOrders()
        {
            var Orders = await _context.Orders.ToListAsync();
            return Orders;
        }

        public async Task<Orders> InsertOrder(Orders order)
        {
            var orders = _context.Orders.Add(order);
            await _context.SaveChangesAsync();
            return orders.Entity;
        }

        public async Task<Orders> DispatchOrder(DispatchOrderDTO in_order) 
        {
            var order = await _context.Orders.FirstOrDefaultAsync(x => x.OrderId == in_order.OrderId);
            order.OrderStatus = (OrderStatus)2;
            await _context.SaveChangesAsync();
            return order;
        }

        public async Task<OrderDTO> CustomMap(Orders order, Product product, Client client, Abcuser user)
        {
            var OrderDto = new OrderDTO
            {
                OrderId = order.OrderId,
                OrderNumber = order.OrderNumber,
                OrderStatus = order.OrderStatus,
                ProductNumber = product.ProductNumber,
                ProductName = product.Name,
                ProductType = product.ProductType,
                ClientNumber = client.ClientNumber,
                ClientName = client.ClientName,
                Name = user.Name,
                SurName = user.SurName,
                LastName = user.LastName,
                UserType = user.UserType
            };
            return OrderDto;
        }
    }
}
