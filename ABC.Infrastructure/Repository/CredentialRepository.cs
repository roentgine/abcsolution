﻿using ABC.Core.Interfaces;
using ABC.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC.Infrastructure.Data.Repository
{
    public class CredentialRepository : IABCCredentialRepository
    {

        private readonly ABCContext _context;
        public CredentialRepository(ABCContext context)
        {

            _context = context;

        }

        public async Task<int> InsertCredential(Credential credential)
        {
            var credentials = _context.Credential.Add(credential);
            await _context.SaveChangesAsync();
            int uid = credentials.Entity.CredentialId;
            return uid;
        }

    }
}
