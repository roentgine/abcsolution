﻿
using ABC.Core.Entities;
using ABC.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace ABC.Infrastructure.Data.Repository
{
    public class ProductRepository : IABCProductRepository
    {

        private readonly ABCContext _context;
        public ProductRepository(ABCContext context)
        {

            _context = context;

        }

        public async Task<IEnumerable<Product>> GetProducts() 
        {
            var products = await _context.Product.ToListAsync();
            return products;
        }


        public async Task<Product> InsertProduct(Product product)
        {
            var in_product = _context.Product.Add(product);
            await _context.SaveChangesAsync();
            return in_product.Entity;
        }

        public async Task<Product> GetProduct(int id)
        {
            var product = await _context.Product.FirstOrDefaultAsync(c => c.ProductId == id);
            return product;
        }

        public async Task<Product> SetProduct(int id) 
        {
            var product = await _context.Product.FirstOrDefaultAsync(c => c.ProductId == id);
            product.ProductStatus = (ProductStatus)2;
            await _context.SaveChangesAsync();
            return product;
        }

    }
}
