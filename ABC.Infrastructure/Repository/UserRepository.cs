﻿using ABC.Core.DTO;
using ABC.Core.Entities;
using ABC.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ABC.Infrastructure.Data.Repository
{
    public class UserRepository : IABCUserRepository
    {

        private readonly ABCContext _context;
        public UserRepository(ABCContext context)
        {

            _context = context;

        }

        public async Task<IEnumerable<Abcuser>> GetUsers()
        {
            var users = await _context.Abcuser.ToListAsync();
            return users;
        }
        
        public async Task<Abcuser> InsertUser(Abcuser user)
        {
            
            var users = _context.Abcuser.Add(user);
            await _context.SaveChangesAsync();
            return users.Entity;
        }

        public async Task<Abcuser> GetUser(int id) 
        {
            var user = await _context.Abcuser.FirstOrDefaultAsync(c => c.AbcUserId == id);
            return user;
        }

        public async Task<Abcuser> CustomMap(InsertUserDTO user, int credentialid) 
        {
            var send_user = new Abcuser
            {
                Name = user.Name,
                SurName = user.SurName,
                LastName = user.LastName,
                UserType = user.UserType,
                CredentialId = credentialid
            };
            return send_user;
        }

    }
}
