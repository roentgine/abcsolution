﻿using ABC.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ABC.Infrastructure.Mappings
{
    public class OrdersMap : IEntityTypeConfiguration<Orders>
    {
        public void Configure(EntityTypeBuilder<Orders> builder)
        {
            builder.ToTable("Orders");
            builder.HasKey(e => e.OrderId)
                     .HasName("PK_Order_OrderId");

            builder.Property(e => e.OrderId).HasDefaultValueSql("(NEXT VALUE FOR [PrimarySequence])");

            builder.Property(e => e.InsertDate)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())");

            builder.Property(e => e.OrderNumber)
                .IsRequired()
                .HasMaxLength(7)
                .HasComputedColumnSql("(concat('OR',right('0000'+CONVERT([nvarchar](5),[OrderId]),(5))))");

            builder.HasOne(d => d.AbcUser)
                .WithMany(p => p.Orders)
                .HasForeignKey(d => d.AbcUserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Order_AbcUserId");

            builder.HasOne(d => d.Client)
                .WithMany(p => p.Orders)
                .HasForeignKey(d => d.ClientId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Order_ClientId");

            builder.HasOne(d => d.Product)
                .WithMany(p => p.Orders)
                .HasForeignKey(d => d.ProductId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Order_ProductId");

            //Implementacion a enum
            builder.Property(e => e.OrderStatus)
            .HasConversion(x => (int)x, x => (OrderStatus)x);
        }    
    }
}
