﻿using ABC.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ABC.Infrastructure.Mappings
{
    public class ClientMap : IEntityTypeConfiguration<Client>
    {
        public void Configure(EntityTypeBuilder<Client> builder)
        {
            builder.ToTable("Client");
            builder.Property(e => e.ClientId).HasDefaultValueSql("(NEXT VALUE FOR [PrimarySequence])");

            builder.Property(e => e.ClientName)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(e => e.ClientNumber)
                .IsRequired()
                .HasMaxLength(7)
                .HasComputedColumnSql("(concat('CL',right('0000'+CONVERT([nvarchar](5),[ClientId]),(5))))");

            builder.Property(e => e.InsertDate)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())");

            //Implementacion a enum
            /*builder.Property(e => e.ProductStatus)
            .HasConversion(x => (int)x, x => (ProductStatus)x);*/

        }    
    }
}
