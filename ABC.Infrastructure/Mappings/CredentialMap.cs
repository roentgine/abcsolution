﻿using ABC.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ABC.Infrastructure.Mappings
{
    public class CredentialMap : IEntityTypeConfiguration<Credential>
    {
        public void Configure(EntityTypeBuilder<Credential> builder)
        {
            builder.ToTable("Credential");
            builder.Property(e => e.CredentialId).HasDefaultValueSql("(NEXT VALUE FOR [PrimarySequence])");

            builder.Property(e => e.InsertDate)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())");

            builder.Property(e => e.Mail)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(e => e.Pass)
                .IsRequired()
                .HasMaxLength(100);
        }    
    }
}
