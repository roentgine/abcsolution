﻿using ABC.Core.DTO;
using ABC.Core.Entities;
using AutoMapper;

namespace ABC.Infrastructure.Mappings
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<Abcuser, UserDTO>()
                .ForMember(u => u.AbcUserId, p => p.MapFrom(pt => pt.AbcUserId))
                .ForMember(u => u.Name, p => p.MapFrom(pt => pt.Name))
                .ForMember(u => u.SurName, p => p.MapFrom(pt => pt.SurName))
                .ForMember(u => u.LastName, p => p.MapFrom(pt => pt.LastName))
                .ForMember(u => u.CredentialId, p => p.MapFrom(pt => pt.CredentialId))
                .ForMember(u => u.UserType, p => p.MapFrom(pt => pt.UserType))
                .ForMember(u => u.InsertDate, p => p.MapFrom(pt => pt.InsertDate))
                .ForAllOtherMembers(u => u.Ignore());

            CreateMap<InsertUserDTO, Credential>()
                .ForMember(u => u.Mail, p => p.MapFrom(pt => pt.Mail))
                .ForMember(u => u.Pass, p => p.MapFrom(pt => pt.Pass))
                .ForAllOtherMembers(u => u.Ignore());

            CreateMap<InsertUserDTO, Abcuser>()
                .ForMember(u => u.Name, p => p.MapFrom(pt => pt.Name))
                .ForMember(u => u.SurName, p => p.MapFrom(pt => pt.SurName))
                .ForMember(u => u.LastName, p => p.MapFrom(pt => pt.LastName))
                .ForMember(u => u.UserType, p => p.MapFrom(pt => pt.UserType))
                .ForAllOtherMembers(u => u.Ignore());


            CreateMap<Client, ClientDTO>()
                .ForMember(u => u.ClientId, p => p.MapFrom(pt => pt.ClientId))
                .ForMember(u => u.ClientName, p => p.MapFrom(pt => pt.ClientName))
                .ForMember(u => u.ClientNumber, p => p.MapFrom(pt => pt.ClientNumber))
                .ForMember(u => u.InsertDate, p => p.MapFrom(pt => pt.InsertDate))
                .ForAllOtherMembers(u => u.Ignore());

            CreateMap<InsertOrderDTO, Orders>()
                .ForMember(u => u.OrderStatus, p => p.MapFrom(pt => pt.OrderStatus))
                .ForMember(u => u.AbcUserId, p => p.MapFrom(pt => pt.AbcUserId))
                .ForMember(u => u.ProductId, p => p.MapFrom(pt => pt.ProductId))
                .ForMember(u => u.ClientId, p => p.MapFrom(pt => pt.ClientId))
                .ForAllOtherMembers(u => u.Ignore());

            CreateMap<InsertProductDTO, Product>()
                .ForMember(u => u.Name, p => p.MapFrom(pt => pt.Name))
                .ForMember(u => u.ProductType, p => p.MapFrom(pt => pt.ProductType))
                .ForMember(u => u.ProductStatus, p => p.MapFrom(pt => 1))
                .ForAllOtherMembers(u => u.Ignore());

            CreateMap<Product, ProductDTO>()
                .ForMember(u => u.ProductId, p => p.MapFrom(pt => pt.ProductId))
                .ForMember(u => u.ProductNumber, p => p.MapFrom(pt => pt.ProductNumber))
                .ForMember(u => u.ProductStatus, p => p.MapFrom(pt => pt.ProductStatus))
                .ForMember(u => u.Name, p => p.MapFrom(pt => pt.Name))
                .ForMember(u => u.ProductType, p => p.MapFrom(pt => pt.ProductType))
                .ForMember(u => u.InsertDate, p => p.MapFrom(pt => pt.InsertDate))
                .ForAllOtherMembers(u => u.Ignore());

            CreateMap<InsertClientDTO, Client>()
                .ForMember(u => u.ClientName, p => p.MapFrom(pt => pt.ClientName))
                .ForAllOtherMembers(u => u.Ignore());

            CreateMap<ClientDTO, Client>()
                .ForMember(u => u.ClientId, p => p.MapFrom(pt => pt.ClientId))
                .ForMember(u => u.ClientName, p => p.MapFrom(pt => pt.ClientName))
                .ForMember(u => u.ClientNumber, p => p.MapFrom(pt => pt.ClientNumber))
                .ForMember(u => u.InsertDate, p => p.MapFrom(pt => pt.InsertDate))
                .ForAllOtherMembers(u => u.Ignore());

        }
    }
}
