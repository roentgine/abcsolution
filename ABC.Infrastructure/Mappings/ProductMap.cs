﻿using ABC.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ABC.Infrastructure.Mappings
{
    public class ProductMap : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Product");
            builder.Property(e => e.ProductId).HasDefaultValueSql("(NEXT VALUE FOR [PrimarySequence])");

            builder.Property(e => e.InsertDate)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(e => e.ProductNumber)
                .IsRequired()
                .HasMaxLength(7)
                .HasComputedColumnSql("(concat('PR',right('0000'+CONVERT([nvarchar](5),[ProductId]),(5))))");

            //Implementacion a enum
            builder.Property(e => e.ProductStatus)
            .HasConversion(x => (int)x, x => (ProductStatus)x);
            
            
            builder.Property(e => e.ProductType)
            .HasConversion(x => (int)x, x => (ProductType)x);
        }    
    }
}
