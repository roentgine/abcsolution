﻿using ABC.Infrastructure.Mappings;
using AutoMapper;
namespace ABC.Infrastructure.AutoMapper
{
    public class MappingConfigurations
    {
        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {   
                cfg.AddProfile(new MapProfile());
            });
        }
    }
}
