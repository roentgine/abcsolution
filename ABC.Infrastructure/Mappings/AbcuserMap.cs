﻿using ABC.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ABC.Infrastructure.Mappings
{
    public class AbcuserMap : IEntityTypeConfiguration<Abcuser>
    {
        public void Configure(EntityTypeBuilder<Abcuser> builder)
        {
            builder.ToTable("Abcuser");
            builder.Property(e => e.AbcUserId).HasDefaultValueSql("(NEXT VALUE FOR [PrimarySequence])");

            builder.Property(e => e.InsertDate)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())");

            builder.Property(e => e.LastName).HasMaxLength(50);

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(e => e.SurName)
                .IsRequired()
                .HasMaxLength(50);

            builder.HasOne(d => d.Credential)
                .WithMany(p => p.Abcuser)
                .HasForeignKey(d => d.CredentialId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_AbcUser_CredentialId");

            //Implementacion a enum
            builder.Property(e => e.UserType)
            .HasConversion(x => (int)x, x => (UserType)x);

        }    
    }
}
