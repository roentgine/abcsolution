﻿using System;
using ABC.Core.Entities;
using ABC.Infrastructure.Mappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ABC.Infrastructure.Data
{
    public class ABCContext : DbContext
    {
        public ABCContext()
        {
        }

        public ABCContext(DbContextOptions<ABCContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Abcuser> Abcuser { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<Credential> Credential { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<Product> Product { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AbcuserMap());
            modelBuilder.ApplyConfiguration(new ClientMap());
            modelBuilder.ApplyConfiguration(new CredentialMap());
            modelBuilder.ApplyConfiguration(new OrdersMap());
            modelBuilder.ApplyConfiguration(new ProductMap());

            modelBuilder.HasSequence<int>("PrimarySequence")
                .StartsAt(0)
                .HasMin(0);
        }
    }
}
