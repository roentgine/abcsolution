﻿
create database ABCDataBase;

CREATE TABLE ABCUser (
    AbcUserId int NOT NULL DEFAULT (NEXT VALUE FOR PrimarySequence),
    Name nvarchar(50) NOT NULL,
    SurName nvarchar(50) NOT NULL,
    LastName nvarchar(50),
    UserType int not null,
    CredentialId int not null,
    InsertDate datetime not null,
    CONSTRAINT PK_AbcUser_AbcUserId PRIMARY KEY (AbcUserId)
);

ALTER TABLE ABCUser ADD CONSTRAINT DF_ABCUser DEFAULT 
GETUTCDATE() FOR InsertDate;

alter table ABCUser 
ADD CONSTRAINT FK_AbcUser_CredentialId FOREIGN KEY (CredentialId)
      REFERENCES Credential (CredentialId)

create table Credential(
    CredentialId int not null DEFAULT (NEXT VALUE FOR PrimarySequence),
    Mail nvarchar(50) not null,
    Pass nvarchar(100) not null,
    InsertDate datetime not null
    CONSTRAINT PK_Credential_CredentialId PRIMARY KEY (CredentialId)
);

ALTER TABLE Credential ADD CONSTRAINT DF_Credential DEFAULT 
GETUTCDATE() FOR InsertDate;

create table Orders(
    OrderId int not null DEFAULT (NEXT VALUE FOR PrimarySequence),
    OrderNumber  AS 
        CONCAT( 'OR',(
        RIGHT('0000' + CAST (OrderId AS nvarchar(5)), 5)
        )) PERSISTED,
    OrderStatus int not null,
    AbcUserId int not null,
    ProductId int not null,
    ClientId int not null,
    InsertDate datetime not null
    CONSTRAINT PK_Order_OrderId PRIMARY KEY (OrderId)
);

ALTER TABLE Orders ADD CONSTRAINT DF_Orders DEFAULT 
GETUTCDATE() FOR InsertDate;

alter table Orders ADD CONSTRAINT FK_Order_ProductId FOREIGN KEY (ProductId)
      REFERENCES Product (ProductId)
      
alter table Orders ADD CONSTRAINT FK_Order_ClientId FOREIGN KEY (ClientId)
      REFERENCES Client (ClientId)
      
alter table Orders ADD CONSTRAINT FK_Order_AbcUserId FOREIGN KEY (AbcUserId)
      REFERENCES ABCUser (AbcUserId)

create table Product(
    ProductId int not null DEFAULT (NEXT VALUE FOR PrimarySequence),
    ProductNumber  AS 
        CONCAT( 'PR',(
        RIGHT('0000' + CAST (ProductId AS nvarchar(5)), 5)
        )) PERSISTED,
    ProductStatus int not null,
    Name nvarchar(100) not null,
    ProductType int not null,
    InsertDate datetime not null
    CONSTRAINT PK_Product_ProductId PRIMARY KEY (ProductId)
);

ALTER TABLE Product ADD CONSTRAINT DF_Product DEFAULT 
GETUTCDATE() FOR InsertDate;

create table Client(
    ClientId int not null DEFAULT (NEXT VALUE FOR PrimarySequence),
    ClientNumber  AS 
        CONCAT( 'CL',(
        RIGHT('0000' + CAST (ClientId AS nvarchar(5)), 5)
        )) PERSISTED,
    ClientName nvarchar(100) not null,
    InsertDate datetime not null
    CONSTRAINT PK_Client_ClientId PRIMARY KEY (ClientId)
);

ALTER TABLE Client ADD CONSTRAINT DF_Client DEFAULT 
GETUTCDATE() FOR InsertDate;

--//////--

Create sequence 
PrimarySequence
as int
start with 0
increment by 1
MINVALUE 0
MAXVALUE 2147483647

--//////--


