﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp1.Entities
{
    public class Client
    {
        public int ClientId { get; set; }
        public string ClientNumber { get; set; }
        public string ClientName { get; set; }
        public DateTime InsertDate { get; set; }
    }
}
