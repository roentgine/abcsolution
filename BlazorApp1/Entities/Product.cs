﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp1.Entities
{
    public class Product
    {
        public int ProductId { get; set; }
        public string ProductNumber { get; set; }
        public string ProductStatus { get; set; }
        public string Name { get; set; }
        public string ProductType { get; set; }
    }
}
