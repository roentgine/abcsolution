﻿    using System.Runtime.Serialization;

namespace ABC.Core.Entities
{
    public enum UserType
    {
        [EnumMember(Value = "Administrador")]
        Administrador = 1,
        [EnumMember(Value = "Vendedores")]
        Vendedores = 2,
        [EnumMember(Value = "Administrativo")]
        Administrativo = 3
    }
    
    public enum OrderStatus
    {
        [EnumMember(Value = "PorDespachar")]
        PorDespachar = 1,
        [EnumMember(Value = "Despachado")]
        Despachado = 2
    }

    public enum ProductStatus 
    {
        [EnumMember(Value = "Disponible")]
        Disponible = 1,
        [EnumMember(Value = "NoDisponible")]
        NoDisponible = 2
    }

    public enum ProductType 
    {
        [EnumMember(Value = "Producto_1")]
        Producto_1 = 1,
        [EnumMember(Value = "Producto_2")]
        Producto_2 = 2
    }

}
