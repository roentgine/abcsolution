﻿using ABC.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ABC.Core.Interfaces
{
    public interface IABCCredentialRepository
    {
        Task<int> InsertCredential(Credential credential);
    }
}
