﻿using ABC.Core.DTO;
using ABC.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ABC.Core.Interfaces
{
    public interface IABCOrdersRepository 
    {
        Task<IEnumerable<Orders>> GetOrders();
        Task<Orders> InsertOrder(Orders order);
        Task<OrderDTO> CustomMap(Orders order, Product product, Client client, Abcuser user);
        Task<Orders> DispatchOrder(DispatchOrderDTO in_order);
    }
}
