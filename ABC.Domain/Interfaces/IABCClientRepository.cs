﻿using ABC.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ABC.Core.Interfaces
{
    public interface IABCClientRepository
    {
        Task<IEnumerable<Client>> GetClients();
        Task<Client> GetClient(int id);
        Task<Client> InsertClient(Client in_client);
    }
}
