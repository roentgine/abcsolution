﻿namespace ABC.Core.Interfaces
{
    public interface IRepository<TEntity> : IRepository<TEntity, int> where TEntity : class
    {
    }
}
