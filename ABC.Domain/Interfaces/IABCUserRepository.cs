﻿using ABC.Core.DTO;
using ABC.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ABC.Core.Interfaces
{
    public interface IABCUserRepository 
    {
        Task<IEnumerable<Abcuser>> GetUsers();
        Task<Abcuser> InsertUser(Abcuser user);
        Task<Abcuser> GetUser(int id);
        Task<Abcuser> CustomMap(InsertUserDTO user, int credentialid);
    }
}
