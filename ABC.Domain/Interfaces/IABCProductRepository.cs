﻿using ABC.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ABC.Core.Interfaces
{
    public interface IABCProductRepository 
    {
        Task<Product> InsertProduct(Product product);
        Task<Product> GetProduct(int id);
        Task<IEnumerable<Product>> GetProducts();
        Task<Product> SetProduct(int id);
    }
}
