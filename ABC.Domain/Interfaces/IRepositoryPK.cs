﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace ABC.Core.Interfaces
{
    public interface IRepository<TEntity, TKey> : IDisposable
        where TEntity : class
    {
        void Add(TEntity obj);
        TEntity GetById(TKey id);
        Task<TEntity> GetByIdAsync(TKey id);
        IQueryable<TEntity> GetAll();
        void Update(TEntity obj);
        void Remove(TKey id);
        Task AddAsync(TEntity entity);
        int SaveChanges();
    }
}
