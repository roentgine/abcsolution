﻿using ABC.Core.Entities;
using System;

namespace ABC.Core.DTO
{
    public class InsertClientDTO
    {
        public string ClientName { get; set; }

    }
}
