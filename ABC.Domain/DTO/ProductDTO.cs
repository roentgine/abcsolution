﻿using ABC.Core.Entities;
using System;

namespace ABC.Core.DTO
{
    public class ProductDTO
    {
        public int ProductId { get; set; }
        public string ProductNumber { get; set; }
        public ProductStatus ProductStatus { get; set; }
        public string Name { get; set; }
        public ProductType ProductType { get; set; }
        public DateTime InsertDate { get; set; }
    }
}
