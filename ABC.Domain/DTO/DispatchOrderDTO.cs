﻿using ABC.Core.Entities;
using System;

namespace ABC.Core.DTO
{
    public class DispatchOrderDTO
    {
        public int OrderId { get; set; }
        public OrderStatus OrderStatus { get; set; }
    }
}
