﻿using ABC.Core.Entities;
using System;

namespace ABC.Core.DTO
{
    public class OrderDTO
    {
        //Pedido
        public int OrderId { get; set; }
        public string OrderNumber { get; set; }
        public OrderStatus OrderStatus { get; set; }
        //Producto
        public string ProductNumber { get; set; }
        public string ProductName { get; set; }
        public ProductType ProductType { get; set; }
        //Cliente
        public string ClientNumber { get; set; }
        public string ClientName { get; set; }
        //Usuario
        public string Name { get; set; }
        public string SurName { get; set; }
        public string LastName { get; set; }
        public UserType UserType { get; set; }
    }
}
