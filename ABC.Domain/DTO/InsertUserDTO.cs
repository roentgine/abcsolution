﻿using ABC.Core.Entities;
using System;

namespace ABC.Core.DTO
{
    public class InsertUserDTO
    {

        public string Name { get; set; }
        public string SurName { get; set; }
        public string LastName { get; set; }
        public UserType UserType { get; set; }
        public string Mail { get; set; }
        public string Pass { get; set; }

    }
}
