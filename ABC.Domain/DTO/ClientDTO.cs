﻿using ABC.Core.Entities;
using System;

namespace ABC.Core.DTO
{
    public class ClientDTO
    {
        public int ClientId { get; set; }
        public string ClientNumber { get; set; }
        public string ClientName { get; set; }
        public DateTime InsertDate { get; set; }
    }
}
