﻿using ABC.Core.Entities;
using System;

namespace ABC.Core.DTO
{
    public class InsertProductDTO
    {
        public string Name { get; set; }
        public ProductType ProductType { get; set; }
    }
}
