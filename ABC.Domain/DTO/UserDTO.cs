﻿using System;

namespace ABC.Core.DTO
{
    public class UserDTO
    {

        public int AbcUserId { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public string LastName { get; set; }
        public int UserType { get; set; }
        public int CredentialId { get; set; }
        public DateTime InsertDate { get; set; }
    }
}
