﻿using ABC.Core.Entities;
using System;

namespace ABC.Core.DTO
{
    public class InsertOrderDTO
    {
        public OrderStatus OrderStatus { get; set; }
        public int AbcUserId { get; set; }
        public int ProductId { get; set; }
        public int ClientId { get; set; }
        
    }
}
