﻿using System;
using System.Collections.Generic;

namespace ABC.Core.Entities
{
    public partial class Product
    {
        public Product()
        {
            Orders = new HashSet<Orders>();
        }

        public int ProductId { get; set; }
        public string ProductNumber { get; set; }
        public ProductStatus ProductStatus { get; set; }
        public string Name { get; set; }
        public ProductType ProductType { get; set; }
        public DateTime InsertDate { get; set; }

        public virtual ICollection<Orders> Orders { get; set; }
    }
}
