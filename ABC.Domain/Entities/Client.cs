﻿using System;
using System.Collections.Generic;

namespace ABC.Core.Entities
{
    public partial class Client
    {
        public Client()
        {
            Orders = new HashSet<Orders>();
        }

        public int ClientId { get; set; }
        public string ClientNumber { get; set; }
        public string ClientName { get; set; }
        public DateTime InsertDate { get; set; }

        public virtual ICollection<Orders> Orders { get; set; }
    }
}
