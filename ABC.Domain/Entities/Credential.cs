﻿using System;
using System.Collections.Generic;

namespace ABC.Core.Entities
{
    public partial class Credential
    {
        public Credential()
        {
            Abcuser = new HashSet<Abcuser>();
        }

        public int CredentialId { get; set; }
        public string Mail { get; set; }
        public string Pass { get; set; }
        public DateTime InsertDate { get; set; }

        public virtual ICollection<Abcuser> Abcuser { get; set; }
    }
}
