﻿using System;
using System.Collections.Generic;

namespace ABC.Core.Entities
{
    public partial class Abcuser
    {
        public Abcuser()
        {
            Orders = new HashSet<Orders>();
        }

        public int AbcUserId { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public string LastName { get; set; }
        public UserType UserType { get; set; }
        public int CredentialId { get; set; }
        public DateTime InsertDate { get; set; }

        public virtual Credential Credential { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }
    }
}
