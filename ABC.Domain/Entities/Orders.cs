﻿using System;
using System.Collections.Generic;

namespace ABC.Core.Entities
{
    public partial class Orders
    {
        public int OrderId { get; set; }
        public string OrderNumber { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public int AbcUserId { get; set; }
        public int ProductId { get; set; }
        public int ClientId { get; set; }
        public DateTime InsertDate { get; set; }

        public virtual Abcuser AbcUser { get; set; }
        public virtual Client Client { get; set; }
        public virtual Product Product { get; set; }
    }
}
